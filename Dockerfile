FROM golang:1.21.4 as builder

WORKDIR /app

COPY . .

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -o unbound-exporter .

RUN strip unbound-exporter

FROM alpine:latest

WORKDIR /root/

COPY --from=builder /app/unbound-exporter .

ENTRYPOINT ["./unbound-exporter"]

